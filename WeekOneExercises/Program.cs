﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeekOneExercises
{
    class Task
    {
        /***                                ****
        ****    Program methods
        ****                                ***/
        // Let the user read task output before displaying the menu again
        public void Continue()    {
            Console.WriteLine("\nPress any key to continue...");
            Console.ReadLine();
        }

        public int InputInteger(String input)    {
            int output = 0;

            if (!int.TryParse(input, out output))   {
                Console.WriteLine("You have failed");
                Console.WriteLine("You entered " + output + "\n");
            }
            else   {        }

            return output;
        }

        public double InputDouble(String input, out bool success)    {
            double output = 0.0;
            success = true;

            if (!double.TryParse(input, out output))    {
                Console.WriteLine("lol");
                success = false;
            }
            return output;
        }

        public int Menu()    {
            String menu = "";

            menu += "* * * * * * * *\n";
            menu += "*    MENU     *\n";
            menu += "* * * * * * * *\n";
            menu += "Please enter a number corresponding to the task you wish to test.\n\n";
            menu += " 0  -  Exit\n";
            menu += " 1  -  HelloName\n";
            menu += " 2  -  Addition\n";
            menu += " 3  -  Division\n";
            menu += " 4  -  Multiplication\n";
            menu += " 5  -  Multiplication Table\n";
            menu += " 6  -  Find perimeter of circle\n";
            menu += " 7  -  Swap two variables\n";
            menu += " 8  -  Positive or negative\n";
            menu += " 9  -  Find greatest number\n";
            menu += "10  -  One dimensional array\n\n";

            Console.WriteLine(menu);
            return InputInteger(Console.ReadLine());
        }

        /***                                ****
        ****    Task methods
        ****                                ***/
        public string HelloName()    {    return "Hello. \nJoesf Millar-Shadgett";    }

        public string Addition()    {
            double n1 = 0, n2 = 0;
            bool isNumber = false;

            Console.WriteLine("Please enter a number.");
            n1 = InputDouble(Console.ReadLine(), out isNumber);
            if (!isNumber)    {
                Console.WriteLine("You failed to enter a number, how embarrassing for you, let's use 10.");
                n1 = 10;
            }

            Console.WriteLine("Please enter a second number.");
            n2 = InputDouble(Console.ReadLine(), out isNumber);
            if (!isNumber)    {
                Console.WriteLine("You failed to enter a number, I dislike you, let's use 1.");
                n2 = 1;
            }
            return "The sum of " + n1 + " and " + n2 + " is " + (n1 + n2);
        }

        public string Division()
        {
            double n1 = 0, n2 = 0;
            bool isNumber = false;

            Console.WriteLine("Please enter a number.");
            n1 = InputDouble(Console.ReadLine(), out isNumber);
            if (!isNumber)    {
                Console.WriteLine("You failed to enter a number, you suck, let's use 5.");
                n1 = 5;
            }

            Console.WriteLine("Please enter a second number.");
            n2 = InputDouble(Console.ReadLine(), out isNumber);
            if (!isNumber)    {
                Console.WriteLine("You failed to enter a number, you mouth-breather, let's use 4321.");
                n2 = 4321;
            }
            return n1 + " divided by " + n2 + " is " + (n1 / n2);
        }

        public string Multiplication()
        {
            double n1 = 0, n2 = 0;
            bool isNumber = false;

            Console.WriteLine("Please enter a number.");
            n1 = InputDouble(Console.ReadLine(), out isNumber);
            if (!isNumber)    {
                Console.WriteLine("You failed to enter a number, are you even trying? let's use 5.");
                n1 = 5;
            }

            Console.WriteLine("Please enter a second number.");
            n2 = InputDouble(Console.ReadLine(), out isNumber);
            if (!isNumber)    {
                Console.WriteLine("You failed to enter a number, you lemon, let's use 14321.");
                n2 = 14321;
            }
            return n1 + " multiplied by " + n2 + " is " + (n1 * n2);
        }

        public string MultiplicationTable()
        {
            double n1 = 0;
            bool isNumber = false;
            string retString = "";

            Console.WriteLine("Please enter a number.");
            n1 = InputDouble(Console.ReadLine(), out isNumber);
            if (!isNumber)    {
                Console.WriteLine("You failed to enter a number... let's use 100.");
                n1 = 100;
            }

            for (int i = 1; i < 10; i++)    {
                retString += n1 + " x " + i + " = " + (n1 * i) + "\n";
            }

            return retString;
        }

        public string FindPerimeterOfCircle()
        {
            double n1 = 0;
            bool isNumber = false;
            string retString = "";

            Console.WriteLine("Please enter a number, which will be the radius of our circle.");
            n1 = InputDouble(Console.ReadLine(), out isNumber);
            if (!isNumber)    {
                Console.WriteLine("You failed to enter a number, doofus. Let's use 67.");
                n1 = 100;
            }
            retString += "Perimeter = " + Convert.ToString(2 * 3.14159265359 * n1) + "\n";
            retString += "Area      = " + Convert.ToString(3.14159265359 * (n1 * n1));

            return retString;
        }

        public string VariableSwap()
        {
            string var1 = "", var2 = "", var3 = "";

            Console.WriteLine("Enter something, it will be variable 1.\n");
            var1 = Console.ReadLine();

            Console.WriteLine("Enter something, it will be variable 2.\n");
            var2 = Console.ReadLine();

            Console.WriteLine("Variable 1 is " + var1);
            Console.WriteLine("Variable 2 is " + var2);

            Console.WriteLine("Swapping variables with black magic!");

            var3 = var1;
            var1 = var2;
            var2 = var3;

            var3 = "Variable 1 is " + var1 + "\n" + "Variable 2 is " + var2;
            return var3;
        }

        public string PositiveOrNegative()
        {
            double var = 0;
            bool isNumber = false;
            string retString = "";

            Console.WriteLine("Please enter a number, I will use my supreme intellect to determine if it is positive or negative.");
            var = InputDouble(Console.ReadLine(), out isNumber);

            if (!isNumber)    {
                Console.WriteLine("You failed to enter a number, I am dissapoint. Let's use 36747.23654.");
                var = 36747.23654;
            }

            if (var > 0)        {    retString = "Number is positive, go you!";                         }
            else if (var < 0)   {    retString = "Number is negative, turn that frown upside down.";    }
            else                {    retString = "Number is 0, neither positive or negative, amazing."; }

            return retString;
        }
    }




    class Program
    {
        static void Main(string[] args)
        {
            bool run = true;
            Task task = new Task();

            while (run)
            {
                int res = task.Menu();
                switch (res)
                {
                    case 0:
                        run = false;
                        break;

                    case 1:
                        Console.WriteLine(task.HelloName());
                        task.Continue();
                        break;

                    case 2:
                        Console.WriteLine(task.Addition());
                        task.Continue();
                        break;

                    case 3:
                        Console.WriteLine(task.Division());
                        task.Continue();
                        break;

                    case 4:
                        Console.WriteLine(task.Multiplication());
                        task.Continue();
                        break;

                    case 5:
                        Console.WriteLine(task.MultiplicationTable());
                        task.Continue();
                        break;

                    case 6:
                        Console.WriteLine(task.FindPerimeterOfCircle());
                        task.Continue();
                        break;

                    case 7:
                        Console.WriteLine(task.VariableSwap());
                        task.Continue();
                        break;

                    case 8:
                        //Console.WriteLine(task.VariableSwap());
                        task.Continue();
                        break;

                    case 9:
                        //Console.WriteLine(task.VariableSwap());
                        task.Continue();
                        break;

                    case 10:
                        //Console.WriteLine(task.VariableSwap());
                        task.Continue();
                        break;

                    case 11:
                        // This is for incorrect input
                        break;

                    //default:
                    //    run = false;
                    //    break;
                }

            }


        }
    }
}
